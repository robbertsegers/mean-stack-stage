'use strict';

angular.module('todos')

.controller('TodosController', ['$scope', '$location', '$state', '$stateParams', 'Todos',
    function($scope, $location, $state, $stateParams, Todos) {

        $scope.max = 4;
        $scope.sortToggle = function sortToggle (param) {
            if ($scope.sortby === param) {$scope.sortby = '-' + param;}
            else {$scope.sortby = param;}
        };

        $scope.weightrange = function(count){
            var weightratings = [];
            for (var i = 0; i < count; i++) {weightratings.push(i)}
                return weightratings;
        }

        $scope.tagsearch = function(tag){
            $scope.search = tag;
        }

        $scope.date = new Date();

        // Create new Todo
        $scope.create = function() {
            // Create new Todo object
            var todo = new Todos ({
                name: this.name,
                description: this.description,
                category: this.category,
                weight: this.weight,
                created: $scope.todo.date,
                tags: this.tags.split(" ")
            });
            // Redirect after save
            todo.$save(function(response) {
                $location.path('todos/' + response._id);
                // Clear form fields
                $scope.name = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });

            /*tags.$save(function(response) {
                $location.path('todos/' + response._id);
                // Clear form fields
                $scope.name = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });*/
        };

        // Find a list of Todos
        $scope.find = function() {
            $scope.todos = Todos.query();
            calculateDayAygoForTodos();
        };

        // Find existing Todo
        $scope.findOne = function() {
            $scope.todo = Todos.get({
                todoId: $stateParams.todoId
            });
        };

        // Update existing Todo
        $scope.update = function() {
            var todo = $scope.todo;
            console.log(todo.tags);
            if (!todo.tags){
                todo.tags=[];
            }
                else{
                    todo.tags = todo.tags.split(" ")};
            console.log(todo);
            todo.$update(function() {
                $location.path('todos/' + todo._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

         // Remove existing Todo
        $scope.remove = function(todo) {
            if ( todo ) {
                todo.$remove();
                for (var i in $scope.todos) {
                    if ($scope.todos [i] === todo) {
                        $scope.todos.splice(i, 1);
                    }
                }
            } else {
                $scope.todo.$remove(function() {
                    $location.path('todos');
                });
            }
        };
        // Search for a Todo
        $scope.todoSearch = function(product) {
            $location.path('todos/' + product._id);
        };

        function calculateDayAygoForTodos(){
        $scope.todos.daysAgo = "days";
        console.log($scope.todos.daysAgo);

        $scope.date | dateDiff:todo.created
        //foreach todo in todos
            for (var i = $scope.todos.length - 1; i >= 0; i--) {
                if () {
                    $scope.todos[i].daysAgo = "days";
                } else{

                };
            };
        });
            //maak nieuwe property 'daysAgo' en vul daar de filterwaarde in
         }
    }
])


        // calculate date diff
        .filter('dateDiff', function () {
          var magicNumber = (1000 * 60 * 60 * 24);

          function checkFormat(date) {
            if (!isNaN(date)) {
              return date;
            }
            return new Date(date).getTime();
          }

          return function (toDate, fromDate) {
            if (toDate && fromDate) {
              var to = checkFormat(toDate);
              var from = checkFormat(fromDate);

              var dayDiff = Math.floor((from - to) / magicNumber);
              if (angular.isNumber(dayDiff)){
                return dayDiff + 1;
              }
            }
          };
        });
;
